package com.company;

import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RandomSearchOptimizer extends Optimizer {

    List<Integer> makespans = new ArrayList<>();


    public RandomSearchOptimizer(List<Integer> initialSchedule, List<List<int[]>> jobs) {
        super(jobs, initialSchedule);
    }

    @Override
    public Pair<Integer, List<Integer>> optimize(int n_iterations, Integer n_time_seconds){
        var best = makeSpan(initialSchedule);
        var start_time = System.currentTimeMillis();

        var temp = new ArrayList<>(initialSchedule);
        for (int i = 0; i < n_iterations; i++) {
            Collections.shuffle(temp);
            var makespan = makeSpan(temp);
            if (makespan<best){
                best=makespan;
                solutions.add(temp);
                makespans.add(makespan);
            }

        }

        var time =  Math.abs((System.currentTimeMillis() - start_time) / 1000);
        var best_solution= Pair.with(best, solutions.get(solutions.size()-1));
        this.printBestSchedule(time,best_solution);
        return best_solution;

    }



}
