package com.company;

public class Main {




    public static void main(String[] args) {
	// write your code here
    var json = new BenchmarkReader();

    var benchmarks = json.loads("./benchmark_problems");
    var a_benchmark = benchmarks.get(4);
    int [] experiment_benchmarks = new int[]{4,benchmarks.size()-1,benchmarks.size()-5};


        for (int i = 0; i < experiment_benchmarks.length; i++) {
            System.out.println("Benchmark " + experiment_benchmarks[i]);
            var scheduler = new Scheduler(benchmarks.get(experiment_benchmarks[i]));

            var initial_schedule = scheduler.lifoSchedule();
            System.out.println("With Genetic Algorithm:");
            var geneticOptimizer = new GeneticOptimizer(scheduler.getJobs(),initial_schedule,scheduler,42);
            var solution = geneticOptimizer.geneticSearch(10000,10000,0.01F, 0.2F);
            /*


            System.out.println("Random Optimization Algorithm n=100000:");
            System.out.println("Random Optimization Algorithm n=10000000:");

            System.out.println("With Genetic Algorithm:");
            var geneticOptimizer = new GeneticOptimizer(scheduler.getJobs(),initial_schedule,scheduler,42);
            var solution = geneticOptimizer.geneticSearch(10000,1000,0.0F, 0.1F);

             */
        }

    //new SolutionVisualizer(solution.getValue1(),schedule.getJobs()).visualize();

    /*
    System.out.println(schedule.toString());
    schedule.fifoSchedule();
    System.out.println(schedule.toString());
    schedule.lifoSchedule();
    System.out.println(schedule.toString());
*/
    }
}





