package com.company;

import java.util.ArrayList;
import java.util.List;

public class Benchmark{
    private List<Resource> resources;
    private List<Job> jobs;

    public List<Resource> getResources() {
        return resources;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public List<List<int[]>> getRepresentation(){
        List<List<int[]>> jobList = new ArrayList<>();
        for (Job job: getJobs()){
            List<int[]> operationList = new ArrayList<>();
            for (Operation op : job.getOperations()
                 ) {
                int [] i = {op.getResource(),op.getDuration()};
                operationList.add(i);
            }
            jobList.add(operationList);
        }
        return jobList;
    }

    @Override
    public String toString() {
        StringBuilder benchmark_str = new StringBuilder("n_Jobs: " + getJobs().size() + "| n_Machines: " + getResources().size() + "\n");
        for (Job job: getJobs()
        ) {
            benchmark_str.append("Job ").append(job.getId()).append(":");

            for (Operation op: job.getOperations()
            ) {
                benchmark_str.append(" (").append(op.getResource()).append(", ").append(op.getDuration()).append(")");
//
            }
            benchmark_str.append("\n");
        }
        return benchmark_str.toString();
    }
}
