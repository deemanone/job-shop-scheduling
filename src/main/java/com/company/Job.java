package com.company;

import java.util.List;

public class Job {
    private int id;
    private List<Operation> operations;

    public int getId() {
        return id;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public boolean equals(Job j) {
        return j.getId() == getId();

    }

    public int [] getRepresentation(){

        return null;
    }
}
