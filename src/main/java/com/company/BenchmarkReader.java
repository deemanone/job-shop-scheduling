package com.company;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BenchmarkReader {
    private final Gson gson;

    public BenchmarkReader() {
        this.gson = new Gson();
    }

    public List<Benchmark> loads(String file_path) {
            var files = this.read_files(file_path);
            var benchmarks_json = new ArrayList<Benchmark>();
            files.forEach(file -> {

                try {
                    var reader = new JsonReader(new FileReader(file.toString()));
                    benchmarks_json.add(gson.fromJson(reader, Benchmark.class));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            });
            return benchmarks_json;
        }

    public List read_files(String file_path){
            List result = null;
            try (Stream<Path> paths = Files.walk(Paths.get(file_path))) {
                result = paths
                        .filter(Files::isRegularFile)
                        .collect(Collectors.toList());

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (result == null) {
                result = new ArrayList();
            }
            return result;
        }
    }

